
## System Dependency

- nodejs v6.3+
- npm 3.10+ (not cnpm)
- node-gyp v3.6.2+ (suggested)
- sqlite v3.8.2+
- g++
- libssl

## Installation for ubuntu 14.04.x or higher.

```
# Install dependency package
sudo apt-get install curl sqlite3 ntp wget git libssl-dev openssl make gcc g++ autoconf automake python build-essential -y
# libsodium for ubuntu 14.04
sudo apt-get install libtool -y
# libsodium for ubuntu 16.04
sudo apt-get install libtool libtool-bin -y

# Install nvm
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash
# This loads nvm
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" 
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# Install node and npm for current user.
nvm install v8
# check node version and it should be v8.x.x
node --version

# git clone sourece code
git clone https://github.com/AschPlatform/asch && cd asch && chmod u+x aschd

# Install node packages
npm install
```

## Web Wallet

```
cd public/

npm install bower -g
npm install browserify -g
npm install gulp  -g

npm install
# angular chose "angular#~1.5.3 which resolved to 1.5.11 and is required by ASCH"
bower install

npm run build
gulp build-test #This make the front-end files in public dir.
```

## Installation on docker.

[Please install Docker firstly](https://store.docker.com/search?offering=community&type=edition)

```
# pull asch code docker image
docker pull aschplatform/asch:v1.3.0
# run docker and asch
docker run -i -t --name asch1.3.0 -p 4096:4096 aschplatform/asch:v1.3.0 /bin/bash
root@e149b6732a48:/# cd /data/asch && ./aschd start
Asch server started as daemon ...
```

## Run 

```
cd asch && node app.js
or
cd asch && ./aschd start
```
Then you can open ```http://localhost:4096``` in you browser.

## Usage

```
node app.js --help

  Usage: app [options]

  Options:

    -h, --help                 output usage information
    -V, --version              output the version number
    -c, --config <path>        Config file path
    -p, --port <port>          Listening port number
    -a, --address <ip>         Listening host name or ip
    -b, --blockchain <path>    Blockchain db path
    -g, --genesisblock <path>  Genesisblock path
    -x, --peers [peers...]     Peers list
    -l, --log <level>          Log level
    -d, --daemon               Run asch node as daemon
    --reindex                  Reindex blockchain
    --base <dir>               Base directory
```

## Default localnet genesis account

This is the genesis account of localnet and one hundred million ARG in it.  
```js
{
  "address": "ABuH9VHV3cFi9UKzcHXGMPGnSC4QqT2cZ5",
  "publicKey": "116025d5664ce153b02c69349798ab66144edd2a395e822b13587780ac9c9c09",
  "secret": "stone elephant caught wrong spend traffic success fetch inside blush virtual element" // password  
}
```



## License

The MIT License (MIT)

